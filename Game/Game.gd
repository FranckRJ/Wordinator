extends Node2D


signal char_typed


var word_scene: PackedScene = load("res://Game/Word.tscn")


func _ready() -> void:
    spawn_word()


func _input(event: InputEvent) -> void:
    if event is InputEventKey and event.pressed and event.unicode != 0:
        char_typed.emit(event.unicode)


func spawn_word() -> void:
    var word := word_scene.instantiate() as Word
    word.position = $WordSpawner.position

    char_typed.connect(word._on_char_typed.bind())
    word.word_completed.connect(self._on_word_completed.bind())
    word.wrong_char_typed.connect(self._on_wrong_char_typed.bind())

    add_child(word)


func _on_word_completed(word: Word) -> void:
    $Score.add_score($PendingPoints.pending_points)
    $PendingPoints.reset_pending_points()
    word.queue_free()
    spawn_word()


func _on_wrong_char_typed() -> void:
    $PendingPoints.decrease_pending_points(1)
