class_name Word


extends Node2D


signal word_completed
signal wrong_char_typed


@onready var word: String = $"/root/WordList".get_random_word()
var typed_part: int
var last_char_was_error: bool = false


func _ready() -> void:
    update_label()

    var font: Font = $WordLabel.get_theme_default_font()
    var font_size: int = $WordLabel.get_theme_default_font_size()
    var word_size: Vector2 = font.get_string_size(word, 0 as HorizontalAlignment, -1, font_size)
    $WordLabel.position = -(word_size / 2)


func update_label() -> void:
    var typed_str: String = word.substr(0, typed_part)
    var error_str: String = word.substr(typed_part, 1 if last_char_was_error else 0)
    var untyped_str: String = word.substr(typed_part + 1 if last_char_was_error else typed_part)
    $WordLabel.text = "[color=green]%s[/color][color=red]%s[/color]%s" % [typed_str, error_str, untyped_str]


func _on_char_typed(chr: int) -> void:
    if typed_part < word.length():
        if word[typed_part].to_lower() == char(chr).to_lower():
            last_char_was_error = false
            typed_part += 1
            update_label()

            if typed_part == word.length():
                word_completed.emit(self)
        else:
            last_char_was_error = true
            wrong_char_typed.emit()
            update_label()
