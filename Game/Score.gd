extends Node2D


var score: int = 0


func _ready() -> void:
    update_label()


func add_score(points: int) -> void:
    score += points
    update_label()


func update_label() -> void:
    $Score.text = "Score: %d" % score
