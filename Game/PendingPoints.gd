extends Node2D


@export var max_pending_points: int = 10
@onready var pending_points: int = max_pending_points


func _ready() -> void:
    update_label()


func reset_pending_points() -> void:
    pending_points = max_pending_points
    update_label()


func decrease_pending_points(amount: int) -> void:
    if pending_points > 1:
        pending_points -= amount
        update_label()


func update_label() -> void:
    $PendingPoints.text = "(+%d)" % pending_points
