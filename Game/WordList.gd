extends Node


var word_list: Array[String]


func _ready() -> void:
    var file := FileAccess.open("res://Game/liste_francais.txt", FileAccess.READ)
    var new_word = file.get_line()
    while not new_word.is_empty():
        if new_word.length() >= 3 and not new_word.contains(" "):
            word_list.append(new_word)
        new_word = file.get_line()


func get_random_word() -> String:
    return word_list[randi_range(0, word_list.size() - 1)]
